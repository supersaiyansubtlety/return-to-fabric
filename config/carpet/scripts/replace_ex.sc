__config() -> {
  'commands' -> {
     '<from> <to> <block> <replacement>' -> '__replace_ex'
   },
   'arguments' -> {
        'from' -> { 'type' -> 'pos' },
        'to' -> { 'type' -> 'pos' },
        'replacement' -> { 'type' -> 'block', 'suggest' -> ['air'] }
   }
};

__replace_ex(from, to, block, replacement)->(
    i=0;
    volume(from, to,
       without_updates(if(_!=block,
          set(pos(_), replacement);
       );
       i+=1;
       if (i % 10000 == 0, game_tick());
    ));
    str('Block: %s was found %s times', block, i)
);