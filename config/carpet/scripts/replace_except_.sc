//A script to find all blocks of a specific type in a cubic area defined by a radius
//locate prints the first 100, and you can tp to those locations
//hist gives a distribution over defined y values
//By: Ghoulboy
__command()->(print(''));

locate(r,cx,cy,cz,block, replacement)->(
	i=0;
	scan([cx, cy, cz], [r, r, r],
		if(_!=block,
			if(i<1000000,
			    pos = pos(_);
                set(pos, replacement);
			);
			i+=1
		)
	);
	'Block: '+block+' was found '+i+' times'
);

