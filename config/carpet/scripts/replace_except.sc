__config() -> {
  'commands' -> {
     '<radius> <pos> <block> <replacement>' -> '__replace_except'
   },
   'arguments' -> {
        'radius' -> { 'type' -> 'int', 'min' -> 1, 'max' -> 128, 'suggest' -> [16, 32, 64] },
        'block' -> { 'type' -> 'blockpredicate' },
        'replacement' -> { 'type' -> 'block', 'suggest' -> ['air']  }
   }
};

__replace_except(r, pos, block, replacement)->(
    i=0;
    scan(pos, [r, r, r],
       if(_!=block && i < 1000000,
          pos = pos(_);
          set(pos, replacement);
       );
       i+=1;
       if (i % 100000 == 0, game_tick());
    );
    str('Block: %s was found %s times', block, i)
);