# Remove all scoreboards
 scoreboard objectives remove EffectNames

# Remove all SurvivalModified scores
scoreboard players reset #sm_effect_names_installed

# Disable module
datapack disable "file/SM Effect Names v1.0.0"

# Message
tellraw @s [{"text":"[SurvivalModified] ","color":"light_purple"},{"text":"Uninstall of [Effect Names] successful.","color":"green"}]
