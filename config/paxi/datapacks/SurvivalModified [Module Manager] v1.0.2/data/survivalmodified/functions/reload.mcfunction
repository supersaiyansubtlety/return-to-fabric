# Prepare Reload
execute unless score #sm_timer_1_second SurvivalModified matches 0..20 run scoreboard players set #sm_timer_1_second SurvivalModified 20
execute unless score #sm_timer_1_minute SurvivalModified matches 0..60 run scoreboard players set #sm_timer_1_minute SurvivalModified 60

# List modules
execute if score #sm_base_list_modules SurvivalModified matches 1 run function survivalmodified:list_modules

# Reset to standard if nothing selected in settings
execute if score #sm_base_afk_team_color SurvivalModified matches 0 run function survivalmodified:setup/afk_display/reset_team_color
execute if score #sm_base_afk_prefix_color SurvivalModified matches 0 run function survivalmodified:setup/afk_display/reset_prefix_color
