# Set Color, finish install
scoreboard players set #sm_base_afk_team_color SurvivalModified 5
team modify sm_team_afk color dark_blue

# Message 
tellraw @a [{"text":"[SurvivalModified Setup] ","color":"light_purple"},{"text":"AFK Team Color: ","color":"green"},{"text":"[Dark Blue]","color":"dark_blue"},{"text":".","color":"green"}]
