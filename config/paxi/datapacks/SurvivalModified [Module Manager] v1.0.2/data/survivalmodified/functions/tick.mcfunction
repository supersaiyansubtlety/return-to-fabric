#Countdown Timer
scoreboard players remove #sm_timer_1_second SurvivalModified 1
execute if score #sm_timer_1_second SurvivalModified matches 0 run function #survivalmodified:second
execute if score #sm_timer_1_second SurvivalModified matches 0 run scoreboard players set #sm_timer_1_second SurvivalModified 20

#Run all tick.mcfunctions in Modules
function #survivalmodified:tick
