# call this to generate a new random number
function sm_prng:calculate_random_number

# the raw random number is stored in #sm_random
execute if score #sm_prng_max_result SurvivalModified matches 0 run tellraw @p [{"text":"[SurvivalModified PRNG] ","color":"light_purple"},{"text":"Calculated random number is ","color":"aqua"},{"score":{"name":"#sm_prng_result","objective":"SurvivalModified","color":"gold"}},{"text":".","color":"aqua"}]
# the value in your specified range is stored in #sm_random_in_range
execute if score #sm_prng_max_result SurvivalModified matches 1.. run tellraw @p [{"text":"[SurvivalModified PRNG] ","color":"light_purple"},{"text":"Calculated random number is ","color":"aqua"},{"score":{"name":"#sm_prng_result_in_range","objective":"SurvivalModified","color":"gold"}},{"text":".","color":"aqua"}]
