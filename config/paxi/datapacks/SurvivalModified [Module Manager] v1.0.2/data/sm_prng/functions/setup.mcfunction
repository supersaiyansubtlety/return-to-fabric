#Run setup if not run yet
execute unless score #sm_prng_setup_finished SurvivalModified matches 1 run function sm_prng:setup/setup_scores
execute if score #sm_prng_setup_finished SurvivalModified matches 1 run function sm_prng:reload
