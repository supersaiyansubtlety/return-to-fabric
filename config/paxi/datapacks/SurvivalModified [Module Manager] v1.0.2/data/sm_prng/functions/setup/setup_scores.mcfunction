# Constants for (ax + c) % m
scoreboard players set #sm_prng_m SurvivalModified 101
scoreboard players set #sm_prng_a SurvivalModified 1653
scoreboard players set #sm_prng_c SurvivalModified 9361

# Turn negative seeds posivite
scoreboard players set #sm_negative_one SurvivalModified -1

# For a certain RNG-Range change #sm_max_result to [Desired Number of Results -1]. If you want 17 different results, you set it to 16.
scoreboard players set #sm_prng_max_result SurvivalModified 0

# Setup finished
scoreboard players set #sm_prng_setup_finished SurvivalModified 1