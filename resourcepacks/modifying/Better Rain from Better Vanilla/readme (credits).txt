Better Vanilla Release 1.7.1b for Minecraft 1.13 | Newest version at http://bit.ly/BetterVanilla | Permission: https://imgur.com/a/UjBMg


Credits 
ESS Addon by Palladium Knight as base - Permission Granted
https://www.planetminecraft.com/texture_pack/ds-advance-enhance-your-minecraft-experience-17-to-111/

Red's Pack by Justin Redfield for paintings - Permission Granted
https://www.planetminecraft.com/texture_pack/redcraft-textures/

Naturus for cactus and sounds - CC BY-NC-SA
http://www.minecraftforum.net/forums/mapping-and-modding-java-edition/resource-packs/2362523-naturus-finally-updated-to-1-11-10-11-16-make-your

Taunoa Complete for lily pad (edited) - Permission Granted
http://www.planetminecraft.com/texture_pack/taunoa-resource-pack---by-zaliku-3690704/

Sensecraft by SCtester for clear water, netherrack, quartz ore, colormap, rain (edited), and magma - Permission Granted
http://www.planetminecraft.com/texture_pack/sensecraft---a-simple-pleasant-texture-pack-which-improves-upon-the-default-experience/

Flashy Redstone by Water_Gem [Noah] and Coti5432 for Redstone, Pistons, and Observers - Permission Granted
https://discord.gg/PNCmPdc

Stone & Ore Variants by Water_Gem [Noah] and Coti5432 for Stone - Permission Granted
https://discord.gg/PNCmPdc

Better Turf by Evtema3 for grass, podzol, and snowy grass (edited) - Permission Granted
https://www.youtube.com/watch?v=rS5SOfHctxI

Default Plus by u/Wert4Nines for smaller crosshair, fishing rod, watermelon, saplings, and language changes - Permission Granted
https://www.reddit.com/r/Minecraft/comments/4wh7wl/my_default_plus_texture_pack_goal_is_to_keep_the/

u/Roelof1337's Fixed Iron Golen for fixed Iron Golem - Permission Granted
https://www.reddit.com/r/Minecraft/comments/5p5sgv/i_fixed_the_iron_golem_texture_oversights/

u/MrVerece's remade recipe book texture for better recipe book - Permission Granted
https://www.reddit.com/r/Minecraft/comments/6gn47s/remade_the_recipebook_texture_to_fit_normal_books/

Unity by CyanideX for all crops and arrows - Permission Granted
https://minecraft.curseforge.com/projects/unity

Naturalized by Jeffyjeff_yt for logs and planks - Permission Granted
https://minecraft.curseforge.com/projects/naturalized

PE Pistons by Mickey Joe Alpha for Pistons - Permission Granted
https://www.reddit.com/r/minecraftsuggestions/comments/5ps2up/update_the_piston_model_to_use_match_pes/

Better Atmosphere by u/creamcheese7 for sky (edited) and lightmap - Permission Granted
https://www.reddit.com/user/creamcheese7/submitted/

Pumpkin Patch by u/CodenameAwesome for fixed pumpkin - Permission Granted
https://www.reddit.com/r/Minecraft/comments/6si42d/a_simple_model_edit_that_makes_pumpkins_uncarved/

Item Frame Fix by Duke for Item Frames - Permission Granted
https://discord.gg/UEz6kKK


Licences
Naturus - CC BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode


Todo
3D ladder & rails
better enchanted books
unique acacia leaves and dark oak leaves 
add alternate flowers & ores
add swampy water
change nether blocks to a more faithful version
pe port
add additional menu panoramas
update old textures
smooth transitions?


Changelog
v1.7.1b (11/18)
Temporary fix for 1.13.2 - Restored Optifine lightmaps and sky (models still yet to be re-added)

v1.7.1 (7/28)
Fixed Painting issue
New Panorama

v1.7 (7/25/18)
Removed Naturus bookshelf
Removed all Naturus sounds besides thunder-no more annoying bird sounds!
Removed pointless texts folder
Changed Naturus planks to a more faithful version (Naturalized Pack)
Resized Tanoa lily pads to 16x16
Changed pack titling/version style
Removed Sensecraft water in favor of new default 1.13 water

1.6 (12/29)
Added Menu Panorama
Fixed Item Frame inconsistency

1.5 (11/11)
Edited readme
Changed 1.8 stones to fit the feel of Minecraft more
Changed Pumpkin model to not be carved when on the ground
Updated some old textures
Removed Dispenser, Dropper, and Furnace models

1.4 (11/8)
Edited readme
Fixed paintings inconsistency

1.3 (11/7)
Sky is not always green anymore! 
Edited readme

1.2 (11/6)
Added Arrows from Unity
Edited readme

1.1 (10/8)
Fixed Diamond Ore
Edited readme

1.0 (9/23/17)
Removed Better Bow
Removed Sky
Removed Bdubs enchantment glint
Removed alternate flowers and ores
Removed ladder and crafting table models
Removed Cauldron Models
Added Colormap from Sensecraft
Added logs from Naturalized
Added PE pisons
Added seperate dark oak leaf textures
Added Sky, fog, and lightmap from Better Atmosphere
Added better rain from sensecraft
Added alternate grass, podzol, and mycelium
Added default plus saplings
Changed 1.8 stones to Default Plus

0.9.98 (7/9)
Added Sky from DokuCraft.
Replaced crops with crops from Unity.
Added colormap from Ozian's Resourcepack
Added Bdub's enchantment glint.

v0.9.95 (6/13)
Removed Glowstone.
Added Bdub's wheat, beetroots, potatoes, carrots, and hay bales.
Fixed flowers and grass for 1.12. 

v0.9.9 (6/12)
Removed easter egg, it didn't work :(
Added Better Recipe Book by MrVerece
Removed Sky Water Demo Pack.
Added Stone Variants from DS Advance.
Added Glowstone from DS Advance. 

v0.9.8 (5/8)
Added an easter egg
Added better iron golem by Roelof1337
Changed pack description.

v0.9.7 (5/6)
reverted fancy leaves.
added custom biome sky colors
added better fishing rod
changed melons to Watermelons
changed bottles o enchanting to EXP Bottles
changed seeds to Wheat Seeds
added better bow
added some cool splashes

v0.9.6 (3/26)
added lower shield

v0.9.5 (3/26)
added smaller crosshair

v0.9.4 (3/24)
removed ItemBound support and changed numbering system.

v0.9.3 (3/22)
added Better Turf!

v0.9.2 (3/22)
added Conected Polished Stones, and Stone variants.

v0.9.1 (3/20)
added flashy redstone and sounds from Naturus.

v0.8 (2/26)
added nether blocks from Sensecraft, and removed custom background. Also removed paper cut out particles.

v0.7.9 (2/20/17)
Removed custom grass blocks.

v0.7.8 (2/19)
Added grass and dirt from Vaders!

v0.7.5 (2/19)
Removed previous grass, ferns, and dirt, added flowers from Vaders!

v0.7 (2/19/17)
Added leaves, grass, ferns, and dirt from Tanoa!
