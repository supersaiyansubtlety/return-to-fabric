
## Visual Sapling Growth
Pack by: MrZeffy



Pack Line: MrZeffyEdits



This pack is intended to be used on top of existing resource packs (or default)

The way visual sapling growth achieves it's effects is by editing the blockstates file for saplings to define variants for growth stages
  Once growth stages have been defined for stages 0 & 1 we now create a new block model file for stage 0 and keep the original name for stage 1
    We recognize that the saplings are pulling their model from the parent "cross.json" block model, we cannot edit this file as it's used by other blocks
    So we create 2 new unique block model parents and call them cross_large.json & cross_small.json
      cross_large.json increases the block model by roughly 25% from default
      cross_small.json decreases the block model by roughly 25% from default
        We now reference the normal <tree>_sapling.json files to cross_large.json and create <sapling>_stage_0.json files
          within these new block models we reference the newly created cross_small.json

Now we have blockstate files that reference 2 block models for each sapling based on growht stage
Each blockstate file references a unique parent file so it doesn't disrupt additional vanilla blocks
Those parent models increase and decrease the model

No textures are needed to achieve this effect, so the pack can easily be placed on top of other resource packs that replace original sapling textures




Pack Structure:

|   pack.mcmeta
|   pack.png
|
\---assets
    \---minecraft
        +---blockstates
        |       acacia_sapling.json
        |       birch_sapling.json
        |       dark_oak_sapling.json
        |       jungle_sapling.json
        |       oak_sapling.json
        |       spruce_sapling.json
        |
        \---models
            \---block
                    acacia_sapling.json
                    acacia_sapling_stage_0.json
                    birch_sapling.json
                    birch_sapling_stage_0.json
                    cross_large.json
                    cross_small.json
                    dark_oak_sapling.json
                    dark_oak_sapling_stage_0.json
                    jungle_sapling.json
                    jungle_sapling_stage_0.json
                    oak_sapling.json
                    oak_sapling_stage_0.json
                    spruce_sapling.json
                    spruce_sapling_stage_0.json
